<?php
/**
 * Registers the content types of the Compeer website.
 *
 * The Compeer website uses various content types to offer its functionality.
 * This file contains the code that is responsible for registering these content types.
 *
 * @package compeer
 * @since 0.0.1
 */

/**
 * Library that eases the creation of content types..
 */
use PostTypes\PostType;
/**
 * Library that allows a simpler way to add taxonomies..
 */
use PostTypes\Taxonomy;


if ( class_exists( PostType::class ) && class_exists( Taxonomy::class ) ) {
	/**
	 * Registers the YOUR_POST_TYPE Content Type.
	 */
	// $area_coordinator = new PostType( 'YOUR_POST_TYPE' );
	// $area_coordinator->options(
	// 	array(
	// 		'supports'     => array( 'title' ),
	// 		'show_in_rest' => false,
	// 		'public'       => false,
	// 		'show_ui'      => true,
	// 	)
	// );
	// $area_coordinator->icon( 'dashicons-buddicons-buddypress-logo' );
	// $area_coordinator->register();
} else {
	wp_die( 'Could not register the content types, the required libraries are not present.' );
}
