<?php
/**
 * Contains the definition of all the ACF fields used by the Social Justice Network.
 *
 * @since 1.0
 *
 * @package scorpiotek-social-media-blocks
 */

use \StoutLogic\AcfBuilder\FieldsBuilder;

if ( class_exists( FieldsBuilder::class ) ) {
	add_action( 'after_setup_theme', 'FUNCTION_NAME' );
}

